recoder
=======


Назначение
----------

Пакет нужен чтобы чинить "кракозябры" (или "краказябры") в читаемый текст. Например: "õîğîøèé òåêñò" => "хооший текст".


Установка
---------
::

    $ git clone https://bitbucket.org/dkuryakin/recoder.git
    $ cd recoder && python setup.py install

или
::

    $ pip install recoder

Полезные команды
----------------

Использование как консольная тулза.
::

    $ echo "Îñíîâíàÿ Îëèìïèéñêàÿ äåðåâíÿ â" | python -mrecoder [coding]

По умолчанию, coding=utf-8.

Использование в коде
--------------------

Чаще всего с кракозябрами справится такой базовый пример:

.. code-block:: python

    from recoder.cyrillic import Recoder
    rec = Recoder()
    broken_text = u'Îñíîâíàÿ Îëèìïèéñêàÿ äåðåâíÿ â'
    fixed_text = rec.fix_common(broken_text)
    print fixed_text.encode('utf-8')


Если базовый пример не справился, можно поиграться с настройками:

.. code-block:: python

    from recoder.cyrillic import Recoder
    rec = Recoder(depth=4)
    broken_text = u'...'
    fixed_text = rec.fix(broken_text)  # fix работает дольше и сложнее чем fix_common
    ...


Можно использовать частоупотребимые слова (и, на, к, в, ...) как индикатор успеха перекодировки. Но в этом случае текст починится только если в нём есть эти слова:

.. code-block:: python

    from recoder.cyrillic import Recoder
    rec = Recoder(use_plus_words=True)
    ...


Замечания
---------

В данный момент поддерживается только кириллица.

Расширение
----------

Если хочется расширить библиотеку не только кириллицей, предусмотренна удобная тулза:
::

    $ cat some_learning_text.txt | python -mrecoder.builder [coding]

По-умолчанию, coding=utf-8. На stdin подавать текстовку для обучения. На выходе получится 2 файлика: 3grams.json и plus_words.json. Далее всё делается по аналогии с recoder.cyrillic.

Тесты
-----

Тут всё просто:
::

    $ git clone https://bitbucket.org/dkuryakin/recoder.git
    $ cd recoder && python setup.py test

Changelog
---------

v0.1.0
 - Реалиизация базовой функциональности.

v0.2.0
 - Добавлние декодеров. Теперь умеет декодить такие кракозябры (взял примеры на 2cyr.com):
 - - &egrave;&eth;&egrave;&euml;&egrave;&ouml;&agrave;
 - - %D0%A2%D0%BE%D0%B2%D0%B0+%D0%B5+%D0%BA
 - - &#229;&#228;&#237;&#224; &#227;&#238;&#228;&#232;
 - - &#1080;&#1088;&#1080;&#1083;&#1080;&#1094;&#1072;

v0.3.0
 - Добавлена поддержка python3.

v0.3.1
 - Удаление из зависимостей пакета regex.
 - Минорные фиксы.